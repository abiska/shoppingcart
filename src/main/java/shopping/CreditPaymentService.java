/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shopping;

/**
 *
 * @author iuabd
 */
public class CreditPaymentService extends PaymentService{

    @Override
    public void processPayment(double amount) {
        super.processPayment(amount);
        System.out.println("Processing credit amount of " + amount);
    } 
}
