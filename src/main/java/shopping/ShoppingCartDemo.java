/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package shopping;

/**
 *
 * @author iuabd
 */
public class ShoppingCartDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentService debitService = factory.getPaymentService(PaymentServiceType.DEBIT);
        
        //create cart and add products
        Cart cart = new Cart();
        cart.addProduct( new Product("shirt", 50) );
        cart.addProduct( new Product("pants", 60) );
        
        //set debit service and pay
        cart.setPaymentService( debitService );
        cart.payCart();
        
        //set credit service and pay
        cart.setPaymentService( debitService );
        cart.payCart();
       
    }
    
}
