/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shopping;

/**
 *
 * @author iuabd
 */
public class PaymentServiceFactory {
    
    private static PaymentServiceFactory instance; //private object
    
    private PaymentServiceFactory(){} //private default constructor
    
    public static PaymentServiceFactory getInstance() //the method that allows 1 instanciation at a time - Singleton
    {
        if(instance==null)
            instance = new PaymentServiceFactory();
        
        return instance;
    }
    
    public PaymentService getPaymentService(PaymentServiceType type){
    
        switch(type){
                case CREDIT: return new PaymentService();
                case DEBIT: return new PaymentService();
        }
        return null;
    
    }   
}
